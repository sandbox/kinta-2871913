<?php

namespace Drupal\linkback_comment\Plugin\Block;

use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Provides a 'SemanticLinkbacks' block.
 *
 * @Block(
 *  id = "semantic_linkbacks",
 *  admin_label = @Translation("Semantic linkbacks"),
 *  context = {
 *    "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *  }
 * )
 */
class SemanticLinkbacks extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Service to query the entities.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * Service to get list of available fields for entity.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $fieldManager;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The service to query the entities.
   * @param \Drupal\Core\Entity\EntityFieldManager $field_manager
   *   The service to get list of available fields for entity.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The service to work with entity Types.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        QueryFactory $entity_query,
        EntityFieldManager $field_manager,
        EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityQuery = $entity_query;
    $this->fieldManager = $field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'comment_semantic_type_field' => [],
    ] + parent::defaultConfiguration();

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $comment_fields = array_combine(array_keys($this->fieldManager->getFieldMap()['comment']), array_keys($this->fieldManager->getFieldMap()['comment']));
    $form['comment_semantic_type_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Semantic field comment machine name'),
      '#description' => $this->t('The comment field that captures the linkback semantic type.'),
      '#default_value' => $this->configuration['comment_semantic_type_field'],
      '#weight' => '0',
      '#options' => ["comment fields" => $comment_fields],
    ];

    $form['show_comments_by_semantic_type'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show list of comments by semantic type'),
      '#default_value' => $this->configuration['show_comments_by_semantic_type'],
      '#description' => $this->t('Uncheck if you want to use a custom view of the list of comments.'),
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access comments');
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['comment_semantic_type_field'] = $form_state->getValue('comment_semantic_type_field');
    $this->configuration['show_comments_by_semantic_type'] = $form_state->getValue('show_comments_by_semantic_type');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->getContextValue('node');
    $nid = $node->id();
    if (!isset($this->fieldManager->getFieldMap()['comment'][$this->configuration['comment_semantic_type_field']])) {
      $build = [];
      $build['semantic_linkbacks']['#markup'] = 'field ' . $this->configuration['comment_semantic_type_field'] . ' not available';
      return $build;
    }
    try {
      $comment_count = $this->entityQuery->getAggregate('comment')
        ->condition('entity_id', $nid)
        ->condition('status', 1)
        ->condition($this->configuration['comment_semantic_type_field'], NULL, 'IS NOT NULL')
        ->aggregate('cid', 'COUNT')
        ->groupBy($this->configuration['comment_semantic_type_field'])
        ->execute();
    }
    catch (QueryException $e) {
      $build = [];
      $build['semantic_linkbacks']['#markup'] = $e->getMessage();
      return $build;
    }
    $build = [];
    $items = [];
    if (!empty($comment_count)) {
      foreach ($comment_count as $key => $semtype) {
        $items[$key]['semantic_type']['name_link'] = [
          '#title' => $semtype[$this->configuration['comment_semantic_type_field']],
          '#type' => 'link',
          '#url' => Url::fromRoute('<current>', [], ['fragment' => 'semantic-comments-' . $semtype[$this->configuration['comment_semantic_type_field']]]),
        ];
	$items[$key]['semantic_type']['anchor'] = new Attribute(['href' => '#semantic-comments-' . $semtype[$this->configuration['comment_semantic_type_field']]]);
	$items[$key]['semantic_type']['name'] = $semtype[$this->configuration['comment_semantic_type_field']];
	$items[$key]['semantic_type']['attributes'] = new Attribute(['class' => $semtype[$this->configuration['comment_semantic_type_field']]]);
	$items[$key]['semantic_type']['icon_class'] = 'icon-' . $semtype[$this->configuration['comment_semantic_type_field']];
        $items[$key]['comments_counter'] = $semtype['cid_count'];
      }
    }
    $build['semantic_comments_counter'] = [
      '#theme' => 'semantic_comments_counter',
      '#items' => $items,
      '#information' => $this->t("This site uses semantic webmentions to interact between sites and build federated conversations."),
    ];
    if ($this->configuration['show_comments_by_semantic_type']) {
      foreach ($comment_count as $key => $semtype) {
        $semantic = $semtype[$this->configuration['comment_semantic_type_field']];
        $comments = $this->entityQuery->get('comment')
          ->condition('entity_id', $nid)
          ->condition('status', 1)
          ->condition($this->configuration['comment_semantic_type_field'], $semantic, "=")
          ->sort('created')
          ->addMetadata('semantic_field', $this->configuration['comment_semantic_type_field'])
          ->addTag('BY_SEMANTIC')
          ->execute();
        $build['semantic_comments_counter']['#comments'][$semantic] = [
          '#type' => 'container',
          '#attributes' => [
            'id' => "semantic-commments-wrapper-" . $semantic,
          ],
        ];
        $build['semantic_comments_counter']['#comments'][$semantic]['title'] = [
          '#type' => 'html_tag',
          '#value' => $semantic,
          '#tag' => 'div',
        ];
        $build['semantic_comments_counter']['#comments'][$semantic]['title']['attributes'] = new Attribute(['id' => 'semantic-comments-' . $semantic]);
        $comment_storage = $this->entityTypeManager->getStorage('comment');
        $comment_objects = $comment_storage->loadMultiple($comments);
        $view_builder = $this->entityTypeManager->getViewBuilder('comment');
        $comments_render = $view_builder->viewMultiple($comment_objects);
        $build['semantic_comments_counter']['#comments'][$semantic]['comments'] = $comments_render;
      }
    }
    return $build;
  }

}
